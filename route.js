const express = require("express");
const app = express.Router();
const bodyParser = require("body-parser");
var cors = require('cors')
const userRoute = require("./modules/user/userRoute");
const notesRoute = require("./modules/notes/notesRoute");
const passport = require('./config/passport');
const url = "/api/v1";

// Body Parser
app.use(bodyParser.json({ limit: "50mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
  })
);

// cors
app.use(cors());

// Routes
app.use(url + "/user", userRoute);
app.use(url + "/notes", notesRoute);

// Http request & Header 
app.all("/*", function (req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Request-Headers", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Headers,x-auth-token"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  return errorUtil.notFound(res, req);
});

module.exports = app;
