const jwt = require('jsonwebtoken');
const data = {};

data.getCurrentUserId = async (data) => {
    var token = data;
    var userID = "";
    if (token) {
        try {
            var decoded = await jwt.decode(token, process.env.JWT_SECRET);
            userID = decoded.id;
        } catch (err) {
            console.log(userID, err);
        }
    }
    return userID;
}

module.exports = data;