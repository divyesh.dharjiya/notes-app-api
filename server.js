const express = require("express");
const app = express();
const bodyParser = require("body-parser");
require("./config/database");
const route = require("./route");

// Body Parser
app.use(bodyParser.urlencoded({ limit: "10gb", extended: true }));
app.use(bodyParser.json({ limit: "10gb" }));

// Main Route
app.use("/", route);

// Server is starts from here
app.listen(process.env.PORT, () => {
  console.log(`server is running on port ${process.env.PORT}`);
});
