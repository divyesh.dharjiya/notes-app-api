const express = require('express');
const middleware = require('../../helper/middleware');
const app = require('../../route');
const notesController = require('./notesController');
const notesRoute = express.Router();

const getNotes = [middleware.checkUserIsLoggedInOrNot, notesController.getNotes];
notesRoute.get('/get-notes', getNotes);

const addNotes = [middleware.checkUserIsLoggedInOrNot, notesController.addNotes];
notesRoute.post('/add-notes', addNotes);

const updateNotes = [middleware.checkUserIsLoggedInOrNot, notesController.updateNotes];
notesRoute.post('/update-notes/:id', updateNotes);

const deleteNotes = [middleware.checkUserIsLoggedInOrNot, notesController.deleteNotes];
notesRoute.delete('/delete-notes/:id', deleteNotes);

module.exports = notesRoute;