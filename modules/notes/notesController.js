const middleware = require("../../helper/middleware");
const getUserId = require('../../helper/getUserId');
const notesModel = require('./notesModel');
const data = require("../../helper/getUserId");

const notesController = {};

notesController.getNotes = async(req, res) => {
    try {
        req.body.userId = req.userData._id;
        let userId = req.body.userId;
        const notesData = await notesModel.find({userId: userId});
        res.status(200).json({
            notesData: notesData,
            totalNotes: notesData.length,
            message: 'Success'
        });
    } catch(error) {
        console.log('error');
    }
};

notesController.addNotes = async(req, res) => {
    try {
        req.body.userId = req.userData._id;
        let notes = await new notesModel({
            userId: req.body.userId,
            title: req.body.title,
            description: req.body.description
        });
        
        let newNote = await notes.save().then((notes) => {
            res.status(200).json({
                notesData: notes,
                message: 'Notes created'
            });
        });
        
    } catch(error) {
        console.log('error');
    }
};

notesController.updateNotes = async(req, res) => {
    try {
        req.body.userId = req.userData._id;
        dataToSave = res.body;
        const updateNote = await notesModel.findOneAndUpdate(
            {_id: req.params.id, userId: req.body.userId},
            {$set: {title: req.body.title, description: req.body.description}}
        );
        if (updateNote) {
            res.status(200).json({
                message: 'Notes Updated'
            });
        } else {
            res.status(401).json({message: 'Not Authorized'});
        }
    } catch(error) {
        console.log('error');
    }
};

notesController.deleteNotes = async(req, res) => {
    try {
        req.body.userId = req.userData._id;
        dataToSave = res.body;
        console.log(req.params.id)
        const deleteNote = await notesModel.deleteOne({_id: req.params.id});
        if (deleteNote) {
            res.status(200).json({
                message: 'Notes Deleted'
            });
        } else {
            res.status(401).json({message: 'Not Authorized'});
        }
    } catch(error) {
        console.log('error');
    }
};

module.exports = notesController;