const mongoose = require('mongoose');

const notesSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "users"
    }, title: {
        type: String,
        required: true
    }, description: {
        type: String,
        required: true
    }, createdt: {
        type: Date,
        default: Date.now()
    }
});

const notesModel = mongoose.model('notes', notesSchema);
module.exports = notesModel;