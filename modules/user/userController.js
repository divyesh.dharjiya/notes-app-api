const userModel = require("./userModel");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("../../config/passport");
const validator = require("../../helper/validator");
const userController = {};

userController.signup = async (req, res) => {
  const { errors, isValid } = validator.checkValidation(req.body);
    if (!isValid) {
    res.status(404).json(errors);
    }
    userModel.findOne({ email: req.body.email }).then((user) => {
    req.body.email = req.body.email.toLowerCase();
      if (user) {
        res.status(404).json("email is already registered.");
      } else {
        const registerNewUser = new userModel({
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          password: req.body.password,
        });
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(registerNewUser.password, salt, (err, hash) => {
            if (err) throw err;
            registerNewUser.password = hash;
            registerNewUser.save().then((user) => {
              res.json(user);
            }).catch((err) => console.log(err));
          });
        });
      }
    });
};

userController.login = async (req, res) => {
  const { errors, isValid } = validator.checkValidation(req.body);
    if (!isValid) {
      res.status(404).json(errors);
    } else {
      userModel.findOne({ email: req.body.email }).then((user) => {
        if (!user) {
          res.status(404).json({ email: "email id does not exist" });
        } else {
          bcrypt.compare(req.body.password, user.password).then((isMatch) => {
            if (!isMatch) {
              res.status(400).json({ error: "Password do not match" });
            } else {
              const payLoad = {
                id: user.id,
              };
              jwt.sign(
                payLoad,
                process.env.JWT_SECRET,
                { expiresIn: 215526 },
                (err, token) => {
                  res.json({
                  success: true,
                  token: token,
                  userData: user,
                  });
                }
              );
            }
          });
        }
      });
    }
};

module.exports = userController;
