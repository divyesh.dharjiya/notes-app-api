const express = require('express');
const app = require('../../route');
const userController = require('./userController');
const middelware = require('../../helper/middleware');
const userRoute = express.Router();

userMiddleware = [middelware.checkEmailIdExistsOrNot, userController.signup];
userRoute.post('/sign-up', userMiddleware);
userRoute.post('/login', userController.login);

module.exports = userRoute;